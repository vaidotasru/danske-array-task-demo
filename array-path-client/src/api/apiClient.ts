import axios from 'axios';
import qs from 'qs';
import { ArrayRequest, ArrayResult } from '../components/ArrayPanel/model';

const api = axios.create({
    baseURL: "http://localhost:5000/api/",
    paramsSerializer: params => qs.stringify(params, { arrayFormat: 'repeat' })
});

export const getShortestPathInArray = async (array: number[], algorithm: string): Promise<ArrayResult> => {
    const result = await api.get('array',
        {
            params: {
                array: array,
                algorithm: algorithm
            } as ArrayRequest
        })
        .then((x) => {
            return x.data as ArrayResult;
        });
    return result;
}