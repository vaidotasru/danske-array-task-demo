import React from 'react';
import './App.css';
import ArrayPanel from './components/ArrayPanel/components/ArrayPanel';
import Header from './components/Shell/Header';

const App: React.FC = () => {
  return (
    <div className="App">
      <Header/>
      <ArrayPanel />
    </div>
  );
}

export default App;
