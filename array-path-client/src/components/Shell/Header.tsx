import React from 'react';

const Header: React.FC = () => {
  return (
    <header className="header">
      <img alt="Danske logo" className="headerIcon" src={window.location.origin + '/danske-bank-logo.svg'}/>
      <h2> Apllication to find shortest array path </h2>
    </header>
  );
}

export default Header;
