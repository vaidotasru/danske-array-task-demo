import React, { useState } from 'react';
import * as apiClient from '../../../api/apiClient';
import { invalidELementCount, invalidElement } from '../../../constants/errors';
import { dp, greedy } from '../../../constants/algorithms';
import { ArrayResult } from '../model';

const ArrayPanel: React.FC = () => {
    const [algorithm, setAlgorithm] = useState<string>(greedy)
    const [array, setArray] = useState<(number | undefined)[]>([])
    const [arrayResult, setArrayResult] = useState<ArrayResult | null>(null);
    const [error, setError] = useState<string | null>(null);
    const [invalidElementIndexes, setInvalidElementIndexes] = useState<number[]>([]);

    const validateInputCount = (count: number | undefined): boolean => {
        if ((count && (count < 1 || count > 1000)) || !count) {
            setError(invalidELementCount);
            setInvalidElementIndexes([]);
            return false;
        }
        if (error === invalidELementCount) {
            setError(null);
        }
        return true;
    }

    const validateElements = (elements: (number | undefined)[]): number[] | null => {
        let localInvalidElementIndexes: number[] = [];

        elements.forEach((x, i) => (typeof (x) !== 'number' || isNaN(x)) && localInvalidElementIndexes.push(i));
        setInvalidElementIndexes(localInvalidElementIndexes);

        if (localInvalidElementIndexes.length > 0) {
            setError(invalidElement);
            return null;
        } else {
            if (error === invalidElement) {
                setError(null);
            }
            return elements as number[];
        }
    }

    const handleArrayValueChange = (newValue: number, index: number) => {
        setArrayResult(null);
        setArray(array.map((x, i) => i === index ? newValue : x));
    }

    const handleArrayCountChange = (count: number | undefined) => {
        setArrayResult(null);
        if (!validateInputCount(count)) {
            setArray([]);
            return;
        }

        if (count! < array.length) {
            setArray(array.slice(0, count));
        }

        if (count! > array.length) {
            let newElements: undefined[] = [];
            for (let step = 0; step < count! - array.length; step++) {
                newElements.push(undefined);
            }
            setArray(array.concat(newElements));
        }

    }

    const handleSubmit = async () => {
        const validatedElements = validateElements(array);
        if (!validatedElements) {
            return;
        }

        const result = await apiClient.getShortestPathInArray(validatedElements, algorithm);
        setArrayResult(result);
    }

    return (
        <div>
            <h5> Select the algorithm </h5>
            <div>
                <label htmlFor={greedy}>Greedy algorithm</label>
                <input id={greedy} type="radio" name="algorithm" onChange={(e) => setAlgorithm(e.target.value)} value={greedy} title="Greedy algorithm" defaultChecked />
            </div>
            <div>
                <label htmlFor={dp}>Dynamic programming</label>
                <input id={dp} type="radio" name="algorithm" onChange={(e) => setAlgorithm(e.target.value)} value={dp} />
            </div>
            <h5> Enter number of elements in the array (1 - 1000) </h5>

            <input className={(error !== invalidELementCount ? "arrayCountInput" : "arrayCountInputError")} type="number" onChange={(e) => handleArrayCountChange(parseInt(e.target.value, 10))} />
            {array.length > 0 && <h5> Fill in the array </h5>}
            {error && <div className="error"> {error} </div>}
            <div className="arrayInputElementsContainer">
                {array.map((x, i) => {
                    return (
                        <div key={i} className="arrayInputElementContainer">
                            <span className="arrayInputElementLabel">
                                <label> {i}: </label>
                            </span>

                            <input
                                className={invalidElementIndexes.indexOf(i) === -1 ? "arrayInputElement" : "arrayInputElementError"}
                                type="number"
                                onChange={(e) =>
                                    handleArrayValueChange(parseInt(e.target.value, 10), i)}
                            />
                        </div>
                    );
                })}
            </div>
            <button className="submitButton" disabled={array.length === 0} onClick={() => handleSubmit()}> Find shortest path </button>

            <div>
                {(arrayResult && arrayResult.route.length === 0) &&
                    <div>
                        <span className="arrayError"> End of array is not reachable </span>
                        <div> <b>Number of iterations: {arrayResult.iterations}</b> </div>
                    </div>
                }
                {(arrayResult && arrayResult.route.length > 0) &&
                    <div>
                        <h4> Result: </h4>
                        {array.map((x, i) => arrayResult.route.indexOf(i) !== -1 ? <span key={i} className="arrayJumpElement"> <b>{x}</b> </span> : <span key={i} className="arrayElement"> {x} </span>)}
                        <div> <b>Number of iterations in the algorithm: {arrayResult.iterations}</b> </div>

                    </div>
                }
            </div>
            <div className="note">Note: If you run same array for the second time, even the algorithm is different, number of iterations will be 0 since the result is cached for 5 minutes </div>
        </div>
    );
}

export default ArrayPanel;
