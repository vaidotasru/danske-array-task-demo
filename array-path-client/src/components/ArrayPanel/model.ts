export interface ArrayRequest {
    array: number[];
    algorithm: string;
}

export interface ArrayResult {
    route: number[];
    iterations: number;
}