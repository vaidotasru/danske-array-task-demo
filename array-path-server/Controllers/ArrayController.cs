﻿using array_path_server.Dtos;
using array_path_server.Helpers;
using array_path_server.Interfaces;
using array_path_server.Constants;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;

namespace array_path_server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArrayController : ControllerBase
    {
        private readonly IArrayService _arrayService;
        private readonly IMemoryCache _cache;

        public ArrayController(IArrayService arrayService, IMemoryCache cache)
        {
            _arrayService = arrayService;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<ArrayResultDto> FindArrayShortestPath([FromQuery] ArrayRequestDto request)
        {
            if (!ValidateRequest(request))
            {
                return BadRequest();
            }

            var arrayStringifiedKey = ArrayStringifier.StringifyIntArray(request.Array); // it is hard and inefficient to use list/array as a key thus we stringify it

            _cache.TryGetValue(arrayStringifiedKey, out List<int> cachedArray);
            if (cachedArray != null)
            {
                return new ArrayResultDto { Route = cachedArray, Iterations = 0 };
            }

            var result = _arrayService.GetShortestArrayPath(request);

            _cache.Set(arrayStringifiedKey, result.Route, new MemoryCacheEntryOptions { AbsoluteExpiration = DateTime.Now.AddMinutes(5)});

            return result;
        }

        private bool ValidateRequest(ArrayRequestDto request)
        {
            if (request.Array.Count > 1000 || request.Array.Count < 1 || 
                !(request.Algorithm == Algorithms.DP || request.Algorithm == Algorithms.Greedy))
            {
                return false;
            }
            return true;
        }
    }
}
