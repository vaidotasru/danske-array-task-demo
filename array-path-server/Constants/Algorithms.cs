﻿namespace array_path_server.Constants
{
    public static class Algorithms
    {
        public const string DP = "DP";
        public const string Greedy = "Greedy";
    }
}
