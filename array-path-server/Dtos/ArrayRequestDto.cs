﻿using System.Collections.Generic;

namespace array_path_server.Dtos
{
    public class ArrayRequestDto
    {
        public List<int> Array { get; set; }
        public string Algorithm { get; set; }
    }
}
