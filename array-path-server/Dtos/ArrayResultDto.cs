﻿using System.Collections.Generic;

namespace array_path_server.Dtos
{
    public class ArrayResultDto
    {
        public List<int> Route { get; set; }
        public int Iterations { get; set; }
    }
}
