﻿using array_path_server.Constants;
using array_path_server.Dtos;
using array_path_server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace array_path_server.Services
{
    public class ArrayService : IArrayService
    {
        readonly List<int> empty = new List<int>();

        public ArrayResultDto GetShortestArrayPath(ArrayRequestDto request)
        {
            var result = new ArrayResultDto { };

            if (request.Algorithm == Algorithms.Greedy)
            {
                result = GetShortestArrayPathGreedy(request.Array);
            }

            if (request.Algorithm == Algorithms.DP)
            {
                result = GetShortestArrayPathDP(request.Array);
            }

            return result;
        } 

        public ArrayResultDto GetShortestArrayPathGreedy(List<int> array)
        {
            var it = 0;
            if (array.Count == 0)
            {
                return new ArrayResultDto { Route = empty, Iterations = it };
            }

            var route = new List<int>() { };
            var pointer = 0;
            while (true)
            {
                it++;
                route.Add(pointer);
                if (array[pointer] + pointer >= array.Count - 1)
                {
                    route.Add(array.Count - 1);
                    return new ArrayResultDto { Route = route, Iterations = it };
                }

                var iterationLimit = (pointer + array[pointer] > array.Count - 1) ? array.Count - 1 : pointer + array[pointer];
                var maximumReach = 0;
                var newPointer = 0;
                for (int i = pointer; i <= iterationLimit; i++)
                {
                    it++;
                    if (maximumReach < i + array[i])
                    {
                        maximumReach = i + array[i];
                        newPointer = i;
                    }
                }

                if (newPointer == pointer)
                {
                    return new ArrayResultDto { Route = empty, Iterations = it };
                }
                pointer = newPointer;
            } 
        }

        public ArrayResultDto GetShortestArrayPathDP(List<int> array)
        {
            var it = 0;
            var empty = new List<int>();

            var minJump = new int[array.Count];
            var jumpIndexes = new int[array.Count];

            for (int i = 1; i < array.Count; i++)
            {
                it++;

                minJump[i] = int.MaxValue;
                jumpIndexes[i] = int.MaxValue;
                for (int j = 0; j < i; j++)
                {
                    it++;
                    if (array[j] >= i - j)
                    {
                        if (minJump[i] > minJump[j] + 1)
                        {
                            minJump[i] = minJump[j] + 1;
                            jumpIndexes[i] = j;
                        }
                    }
                }
            }

            it += minJump.Length; 
            if (minJump.Contains(int.MaxValue))
            {
                return new ArrayResultDto { Route = empty, Iterations = it };
            }

            var nextIndex = array.Count - 1;
            var route = new List<int>
            {
                nextIndex
            };

            while (nextIndex != 0)
            {
                it++;
                nextIndex = jumpIndexes[nextIndex];
                route.Add(nextIndex);
            }

            return new ArrayResultDto { Route = route, Iterations = it };
        }
    }
}
