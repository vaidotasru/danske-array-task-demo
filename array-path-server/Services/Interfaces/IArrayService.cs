﻿using array_path_server.Dtos;
using System.Collections.Generic;

namespace array_path_server.Interfaces
{
    public interface IArrayService
    {
        ArrayResultDto GetShortestArrayPath(ArrayRequestDto request);
        ArrayResultDto GetShortestArrayPathGreedy(List<int> array);
        ArrayResultDto GetShortestArrayPathDP(List<int> array);
    }
}
