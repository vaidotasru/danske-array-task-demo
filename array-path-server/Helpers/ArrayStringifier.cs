﻿using System.Collections.Generic;
using System.Linq;

namespace array_path_server.Helpers
{
    public static class ArrayStringifier
    {
        public static string StringifyIntArray(List<int> Array)
        {
            var stringifiedArrayKey = string.Empty;
            Array.ForEach(x => stringifiedArrayKey += $" {x} ");

            return stringifiedArrayKey;
        }

        public static string StringifyIntArray(int[] Array)
        {
            var stringifiedArrayKey = string.Empty;
            Array.ToList().ForEach(x => stringifiedArrayKey += $" {x} ");

            return stringifiedArrayKey;
        }
    }
}
