using array_path_server.Interfaces;
using array_path_server.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace array_path_test
{
    public class ArrayServiceTests
    {
        private IArrayService _arrayService = new ArrayService();

        [Fact]
        public void TestGreedyValidArray()
        {
            var input = new List<int>() { 1, 2, 0, 3, 0, 2, 0 };
            var expectedOutput = new List<int>() { 0, 1, 3, 6 };

            var output = _arrayService.GetShortestArrayPathGreedy(input);
            Assert.True(output.Route.SequenceEqual(expectedOutput) , "Wrong jumping path for the array");
        }

        [Fact]
        public void TestGreedyInvalidArray()
        {
            var input = new List<int>() { 1, 2, 0, 1, 0, 2, 0 };
            var expectedOutput = new List<int>() {};

            var output = _arrayService.GetShortestArrayPathGreedy(input);
            Assert.True(output.Route.SequenceEqual(expectedOutput), "Jumping path should be empty");
        }

        [Fact]
        public void TestGreedyAdditionalValidArray()
        {
            var input = new List<int>() { 5, 3, 8, 8, 8, 1, 1, 200, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
            var expectedOutput = new List<int>() { 0, 4, 7, 19 };

            var output = _arrayService.GetShortestArrayPathGreedy(input);
            Assert.True(output.Route.SequenceEqual(expectedOutput), "Wrong jumping path for the array");
        }

        [Fact]
        public void TestGreedyAdditionalInvalidArray()
        {
            var input = new List<int>() { 5, 3, 0, 0, 0, 0, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
            var expectedOutput = new List<int>() {  };

            var output = _arrayService.GetShortestArrayPathGreedy(input);
            Assert.True(output.Route.SequenceEqual(expectedOutput), "Jumping path should be empty");
        }

        [Fact]
        public void TestDPValidArray()
        {
            var input = new List<int>() { 1, 2, 0, 3, 0, 2, 0 };
            var expectedOutput = new List<int>() { 6, 3, 1, 0 };

            var output = _arrayService.GetShortestArrayPathDP(input);
            Assert.True(output.Route.SequenceEqual(expectedOutput), "Wrong jumping path for the array");
        }

        [Fact]
        public void TestDPInvalidArray()
        {
            var input = new List<int>() { 1, 2, 0, 1, 0, 2, 0 };
            var expectedOutput = new List<int>() { };

            var output = _arrayService.GetShortestArrayPathDP(input);
            Assert.True(output.Route.SequenceEqual(expectedOutput), "Jumping path should be empty");
        }

        [Fact]
        public void TestDPAdditionalValidArray()
        {
            var input = new List<int>() { 5, 3, 8, 8, 8, 1, 1, 200, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
            var expectedOutput = new List<int>() { 19, 7, 2, 0 };

            var output = _arrayService.GetShortestArrayPathDP(input);
            Assert.True(output.Route.SequenceEqual(expectedOutput), "Wrong jumping path for the array");
        }

        [Fact]
        public void TestDPAdditionalInvalidArray()
        {
            var input = new List<int>() { 5, 3, 0, 0, 0, 0, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
            var expectedOutput = new List<int>() { };

            var output = _arrayService.GetShortestArrayPathDP(input);
            Assert.True(output.Route.SequenceEqual(expectedOutput), "Jumping path should be empty");
        }
    }
}
